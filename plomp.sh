set -e
set -u

commit=${commit:-commit}

dry=""
if [ "$1" = -dry ]; then
	dry=1
	shift
fi

if [ -z "${1:-}" ] || [ "${1#-}" != "$1" ]; then
	echo "<plomp> [-dry] srcbranch [tgtbranch]"
	echo "tgtbranch default: current branch"
	echo "-dry: don't touch tgtbranch, leave result in <srcbranch>_on_<tgtbranch>"
	exit 0
fi

setup() {
	git init --quiet .
	git $commit --quiet -m 'ee' --allow-empty

	git co --quiet master
	git co --quiet -b ab
	echo a > a
	echo b > b
	git add .
	git $commit --quiet -m 'a b'

	git co --quiet master
	git co --quiet -b ac
	echo x > a
	echo c > c
	git add .
	git $commit --quiet -m 'a c'
}

tmpdir() {
	tmp=`git rev-parse --git-dir`/"plomp.$1"."$RANDOM$RANDOM$RANDOM"
	if [ "$tmp" = "${tmp#/}" ]; then
		echo "`pwd`"/"$tmp"
	else
		echo "$tmp"
	fi
}
plomp() {
	what="$1"
	onto="$2"

	od="`tmpdir $onto`"
	git worktree add -b "${what}_on_$onto"       "$od" "$onto"

	wd="`tmpdir $what`"
	git worktree add -b "${what}_on_${onto}_tmp" "$wd" "$what"
	trap "rm -rf $od $wd; git worktree prune" RETURN

	git -C "$od" merge --no-commit "$what" >/dev/null 2>&1 || true # just need the metadata
	(cd "$wd"; rsync --ignore-times --delete --recursive --exclude .git . "$od")
	rm -r "$wd"
	git worktree prune
	git branch --delete --force --quiet "${what}_on_${onto}_tmp"

	git -C "$od" add --all
	git -C "$od" $commit --quiet --message="$what added over $onto"
	rm -r "$od"
	git worktree prune
}

useplomp() {
	md=.
	if [ "${2:-}" != `git rev-parse --abbrev-ref HEAD` ]; then
		md="`tmpdir "$2"`"
		git worktree add "$md" "$2"
		trap "rm -rf $md; git worktree prune" RETURN
	fi
	git -C "$md" merge "$1"_on_"${2:-`git rev-parse --abbrev-ref HEAD`}"
	git -C "$md" branch -d "$1"_on_"${2:-`git rev-parse --abbrev-ref HEAD`}"
}

check() {
	git br -v
	git co --quiet ac_on_ab >/dev/null
	git log
	ls -l
	cat a
}

cleanup() {
	rm -rf * .git
}

dev() {
setup
trap "cleanup" EXIT
git co --quiet ab
plomp ac ab
check
cleanup >/dev/null
}

export GIT_AUTHOR_NAME=codegen
export GIT_AUTHOR_EMAIL=codegen
plomp "$1" "${2:-`git rev-parse --abbrev-ref HEAD`}"
test -z "$dry" && useplomp "$1" "${2:-}"
