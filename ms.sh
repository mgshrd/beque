set -e
set -u

commit=${commit:-commit}

DIR=$(cd "`dirname "${BASH_SOURCE[0]}"`"; pwd)

genfiles() {
perl "$DIR"/ms.pl "$@" -f

echo '_CoqProject
._CoqProject.proofgeneral

.coq-deps
.bin/

Beque/
proviola/

test.hs
test.ml
test.mli
test.pp.ml
test.pp.hs

Beque.Model.'$2'*.html
Beque.Model.'$2'-*.tar.gz' > .gitignore

echo 'import qualified Control.Exception
import qualified Data.Bifunctor
import qualified Data.Char

import qualified System.IO' > imports.hs.inc

echo -n 'type BequeResult ok err = Prelude.Either err ok

coqstring2string :: String -> Prelude.String
coqstring2string s = case s of { EmptyString -> ""; String0 c sx -> c:(coqstring2string sx) }
string2coqstring :: Prelude.String -> String
string2coqstring s = Prelude.foldr (\e accu -> if Data.Char.ord e Prelude.> 255 then Prelude.error ("invalid coq character " Prelude.++ (Prelude.show Prelude.$ Data.Char.ord e)) else String0 e accu) EmptyString s

instance Prelude.Show String where show = coqstring2string
instance Prelude.Read String where readsPrec = \_ s -> [(string2coqstring s, "")]
' > glue-string.hs.inc

echo 'trymap :: Prelude.IO a -> Prelude.IO (Prelude.Either String a)
trymap = ((Prelude.fmap (Data.Bifunctor.first Prelude.$ string2coqstring Prelude.. Prelude.show)) :: (Prelude.Functor f, Data.Bifunctor.Bifunctor g) => f (g Control.Exception.SomeException b) -> f (g String b)) Prelude.. Control.Exception.try

trymap_stderr :: Prelude.IO a -> Prelude.IO (Prelude.Either String a)
trymap_stderr a =
  do
    e <- trymap a
    case e of
      Prelude.Left msg -> System.IO.hPutStrLn System.IO.stderr Prelude.$ Prelude.show msg
      Prelude.Right _ -> Prelude.return ()
    Prelude.return e' > glue-trymap.hs.inc

echo 'let string2coqstring s =
  let r = ref EmptyString in
  for i = 1 to String.length s do
    r := String (String.get s (String.length s - i), !r)
  done;
  !r
let coqstring2string cs =
  let r = Buffer.create 16 in
  let rec loop csx =
    match csx with
    | EmptyString -> ()
    | String (a, s1) -> Buffer.add_char r a; loop s1
  in loop cs; Buffer.contents r' > glue-string.ml.inc

echo 'let trymap = fun v -> fun () -> try Ok (v ()) with e -> Err (string2coqstring (Printexc.to_string e))

let trymap_stderr = fun v -> fun () ->
  let r = (trymap v) ()
  in match r with
  | Err m -> prerr_string (coqstring2string m); prerr_string "\n"; r
  | Ok _ -> (); r' > glue-trymap.ml.inc

touch imports.ml.inc
}

if [ -z "$1" ]; then
	echo 'usage ms.sh [parent module type.v] modulename [action/param:type-param:type,...] [prop/param:type-param:type]' >&2
	exit 1
fi

parent=""
if [ -f "$1" ]; then
	parent="$1"
	shift
elif [ "$1" != "${1%Impl}" ]; then
	parent="`find -name "${1%Impl}"`"
else
	parent="`find "$DIR/" -name Domain.v -print0 | xargs -0 ls -t | head -n 1`"
fi
if [ "$parent" = "${parent#/}" ]; then
	parent="`pwd`"/"$parent"
fi
if [ ! -f "$parent" ]; then
	echo could not find parent >&2
	exit 1
fi

modname=$1
shift

actions="${1:-,}"
shift || true
if [ "$actions" = "`echo $actions | sed -e s/,//`" ]; then
	actions="$actions," # ms.pl expects at least one comma, empty specs dropped
fi

params="${1:-,}"
shift || true
if [ "$params" = "`echo $params | sed -e s/,//`" ]; then
	params="$params," # ms.pl expects at least one comma, empty specs dropped
fi

if [ -n "${1:-}" ]; then
	echo "unknown arg left: "$@"" >&2
	exit 1
fi

sbr=skeleton_$BEQUEVER${skeleton_extra:-}
d="${ddir:-"$modname"}"
new=""
if [ ! -d "$d" ]; then
	new=1
	mkdir $d

	git -C $d init .
	GIT_AUTHOR_EMAIL=codegen GIT_AUTHOR_NAME=codegen git -C $d $commit --quiet --allow-empty -m 'initial empty commit for '$modname
	git -C $d tag repo_root
fi

dd="`pwd`/../${modname}_$RANDOM"
git -C $d worktree add -b $sbr $dd repo_root
trap "cd `pwd` && rm -rf $dd && git -C $d worktree prune" EXIT

pushd $dd

genfiles "$parent" "$modname" "$actions" "$params"

git add .
GIT_AUTHOR_EMAIL=codegen GIT_AUTHOR_NAME=codegen git $commit --quiet -m 'generated skeleton for '"$modname"' with tag '"`echo $sbr | sed -e 's/_dirty-[0-9].*/_dirty-.../'`"

popd

if [ -n "$new" ]; then
	git -C $d rebase $sbr
	git -C $d branch skeleton $sbr
fi

rm -rf $dd
git -C $d worktree prune
git -C $d tag $sbr $sbr
git -C $d branch --delete --force $sbr

echo git rebase --onto $sbr $(git describe --all --exact-match $(git log --grep 'generated skeleton' --format=%h | tail -n 1))
echo $sbr
