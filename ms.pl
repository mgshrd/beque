use strict;

my $usage = "usage: prog <basemodfile> <targetmodname> [action[:resultt][/param:paramt[-param:paramt]...][,action[:resultt][/param:paramt[-param:paramt]...]]...] [prop[/param:paramt[-param:paramt]...][,prop[/param:paramt[-param:paramt]...]...] [-g]";

my $basename = $ARGV[0];
die $usage if $basename eq "";
shift @ARGV;

open my $B, "<$basename" or die "failed to open $basename";

my $modname = $ARGV[0];
die $usage if $modname eq "";
shift @ARGV;

my $lcmodname = lc $modname;
$lcmodname =~ /^(.)/;
my $modactname = "$1am";

sub wrapifnz {
	my $pre = shift;
	my $val = shift;
	my $post = shift;
	return "$pre$val$post" if $val ne "";
	return $val;
}
sub parseparams {
	my $str = shift; # param:paramt-param:paramt...
	
	my @names = ();
	my %types = ();

	for my $p (split /-/, $str) {
		my ($n, $t) = split /:/, $p;
		push @names, $n;
		$types{$n} = $t;
	}

	return (\@names, \%types);
}
sub parseactionprop {
	my $str = shift; # name[:resulttype]/param:paramt-param:paramt,name[:resulttype]/param:paramt...

	my @ap = split /,/, $str;
	my @apnames = map { my $x = $_; $x =~ s/\/.*//; $x } @ap;
	my %apparams = map { split /\//, $_ } @ap;
	my %apparpar = ();
	for my $ape (@apnames) {
		my ($names, $types) = parseparams($apparams{$ape});
		$apparpar{$ape} = {'names' => $names, 'types' => $types};
	}
	return (\@apnames, \%apparpar);
}

sub underscores {
	my $names = shift;
	my $full = join(" ", @{$names});
	$full =~ s/\w+/_/g;
	return $full;
}

my @actions = ();
my %actionrestypes = ();
my %actionpars = ();
sub arest {
	my $aname = shift;
	return $actionrestypes{$aname};
}
sub aparnames {
	my $aname = shift;
	my @res = ();
	@res = @{${$actionpars{$aname}}{names}} if $actionpars{$aname};
	return \@res;
}
sub apartype {
	my $aname = shift;
	my $pname = shift;
	return ${${$actionpars{$aname}}{types}}{$pname};
}
my $actionbak = "";
if ($ARGV[0] =~ /,/) {
	$actionbak = $ARGV[0];
	my ($apn, $app) = parseactionprop($ARGV[0]);

	@actions = map { my $x = $_; $x =~ s/:.*//; $x } @{$apn};
	%actionrestypes = map { my ($k, $v) = split /:/; $v = "unit" unless defined $v; ($k, $v) } @{$apn};
	for my $k (keys %{$app}) {
		my ($nk) = split(/:/, $k);
		$actionpars{$nk} = ${$app}{$k};
	}

	shift @ARGV;
}

sub uniq {
	my %tmp = map { $_ => 1 } @_;
	my @tmp2 = sort keys %tmp;
	return @tmp2;
}
my @actionparamtypes = uniq (map { my $a = $_; map { apartype($a, $_) } @{aparnames $a} } @actions);
my $actionparamtypeimports = wrapifnz("", join("", map { "Require Import $_.\n" } @actionparamtypes), "\n");

my @props = ();
my %proppars = ();
sub pparnames {
	my $pname = shift;
	my @res = ();
	@res = @{${$proppars{$pname}}{names}} if $proppars{$pname};
	return \@res;
}
sub ppartype {
	my $pname = shift;
	my $ppname = shift;
	return ${${$proppars{$pname}}{types}}{$ppname};
}
my $propbak = "";
if ($ARGV[0] =~ /,/) {
	$propbak = $ARGV[0];
	my ($apn, $app) = parseactionprop($ARGV[0]);

	@props = @{$apn};
	%proppars= %{$app};

	shift @ARGV;
}

my $full = "";
if ($ARGV[0] eq "-f") {
	$full = 1;

	shift @ARGV;
}

my $dbg = "";
if ($ARGV[0] eq "-g") {
	$dbg = 1;

	shift @ARGV;
}

my $M;
my $A;
my $Mi;
my $Ai;
my $E;
my $T;
my $MF;
my $GL;
if ($full and $modname ne "-") {
	mkdir "src" or die $!;
	mkdir "src/impl" or die $!;
	mkdir "test" or die $!;

	open $M, ">", "src/$modname.v" or die $!;
	open $A, ">", "src/${modname}Actions.v" or die $!;
	open $Mi, ">", "src/impl/${modname}Impl.v" or die $!;
	open $T, ">", "test/TestHaskell.v" or die $!;
	#open $Ai, ">", "src/impl/${modname}ActionsImpl.v" or die $!;
	#open $E, ">", "src/${modname}Embed.v" or die $!;
	open $MF, ">", "Makefile" or die $!;
	open $GL, ">", ".gitlab-ci.yml" or die $!;
} else {
	open $M, ">-" or die $!;
	open $A, ">-" or die $!;
	open $Mi, ">-" or die $!;
	open $Ai, ">-" or die $!;
	open $E, ">-" or die $!;
	open $MF, ">-" or die $!;
}

sub impls {
	my $res = shift;
	my $impl = "";
	if (/^Parameter action /) {
		$impl = "  exact ${modactname}.${lcmodname}_action.\n";
	}
	if (/^Parameter eq_action_dec /) {
		$impl = "  exact ${modactname}.eq_${lcmodname}_action_dec.\n";
	}
	if (/^Parameter action_res /) {
		$impl = "  exact ${modactname}.${lcmodname}_action_res.\n";
	}
	if (/^Parameter eq_action_res_dec /) {
		$impl = "  exact ${modactname}.eq_${lcmodname}_action_res_dec.\n";
	}
	if (/^Parameter prop /) {
		my $pd = join("", map { my $a = $_; "\n| ${lcmodname}_prop__$_ : ".join("", map { ppartype($a, $_)." -> " } @{pparnames $_})."${lcmodname}_prop" } @props);
		print $res "Inductive ${lcmodname}_prop :=$pd.\n";
		$impl = "  exact ${lcmodname}_prop.\n";
	}
	if (/^Parameter eq_prop_dec /) {
		$impl = "  decide equality.\n"
	}
	if (/^Parameter (back_)?propagates /) {
		my $pd = join("", map { my $a = $_; map { "         | ${lcmodname}_prop__$_".wrapifnz(" ", underscores(pparnames $_), "").", $modactname.${lcmodname}_action__$a".wrapifnz(" ", underscores(aparnames $a), "").", r => False\n" } @props } @actions);
		$impl =
		"  intros.\n".
		"  exact (match p, a, res with\n".
		$pd.
		"         end).\n"
	}
	if (/^Parameter (back_)?propagates_dec /) {
		$impl =
		"  intros.\n".
		"  destruct a, p; firstorder.\n";
	}
	if (/^Parameter prop_to_trace_prop /) {
		my $md = join("", map { "         | ${lcmodname}_prop__$_".wrapifnz(" ", underscores(pparnames $_), "")." => fun _ => False\n" } @props);
		$impl =
		"  intros ?.\n".
		"  exact (match p with\n".
		$md.
		"         end).\n"
	}
	if (/^Parameter action_to_IO /) {
		print $res "Module ea := ExistentialAction w ior.
Definition plain_action :=
  ea.plain_action
    action_res
    segment_of_action_with_res
    (fun _ => True) (* all actions callable all the time *).\n\n";

		my $ad = join("\n", map { my $a = $_; "Axiom $_:
  ".wrapifnz("forall\n    ", join("\n    ", map { my $t = apartype $a, $_; "($_ : $t)" } @{aparnames $_}), ",\n  ")."plain_action
      (${modactname}.${lcmodname}_action__$_".wrapifnz(" ", join(" ", @{aparnames $_}), "").").\n" } @actions);
		print $res "$ad\n";

		my $md = join("", map { "  - refine (existT _ (_, _) ($_".wrapifnz(" ", underscores(aparnames $_), "").", _)); [ solve [ auto ] ].\n" } @actions);
		$impl =
		"  intros.\n".
		"  destruct a.\n".
		$md;
	}
	if (/^Conjecture (back_)?propagates_correct /) {
		$impl = "  destruct p.\n";
	}

	return $impl;
}

sub genmod {
	my $modname = shift;
	my $base = shift;
	my $res = shift;
	die "base undef" unless $base;
	die "res undef" unless $res;
	my $state = "";
	my $mtname = "";
	my $mtargs = "";
	my $paend = "";
	my $impl = "";
	print $res $actionparamtypeimports unless $modname =~ /Impl$/;
	while (<$base>) {
		chomp;
	
		if ($state eq "modbodystart") {
			$state = "modbody";
			print $res "Module ${modactname} := ${modname}Actions.\n" unless $modname =~ /Impl$/;
		}
		if ($state eq "modparams" && /\.$/) {
			print $res "<: $mtname$mtargs\n";
			$state = "modbodystart";
		}
		if ($state eq "modparams" && /\(([^ ]*) :/) {
			$mtargs = "$mtargs $1";
		}
		if ($state eq "modparams" && /<: /) {
			next; #skip inherite module types as they are transitively included through $mtname
		}
		if (s/Module Type ([^ ]*)/Module Type $modname/) {
			$state = "modparams";
			$mtname = $1;
			$mtargs = "";
			print $res "Require Import Beque.Util.DelayExtractionDefinitionToken.\n\n" if $modname =~ /Impl$/;
			print $res "Require Import Beque.Frame.util.ExistentialAction.\n\n" unless $modname =~ /Impl$/;

			print $res "Require Import (*PACKAGE.*)$mtname.\n\n";

			print $res "Require Import ${modname}Actions.\n\n(** * $modname summary *)\n" unless $modname =~ /Impl$/;
			s/Module Type/Module/ if $modname =~ /Impl$/;
		}

		if (s/^End $mtname\.$/End $modname./ and $modname =~ /Impl$/) {
			my $ed = join("\n", map { 'Extract Constant '.$_.' => "\\'.join(" ", @{aparnames $_}).' -> XXX_MISSING_IMPL_'.$_.'".'."\n" } @actions);
			print $res "Module extractHaskell (token : DelayExtractionDefinitionToken).\n".
			$ed.
			"End extractHaskell.\n".
			"\n".
			"Module extractOcaml (token : DelayExtractionDefinitionToken).\n".
			$ed.
			"End extractOcaml.\n".
			"\n";
		}
	

		my $impltmp = impls $res, $_;
		$impl = $impltmp if $impltmp ne "";

		if (s/^Conjecture ([^ ]*)/Lemma $1/) {
			$state = "pastart";
			$paend = "Qed";
		}

		s/^Parameter (segment_of_action_with_res) /Axiom $1/ if 1;

		if (s/^Parameter ([^ ]*)/Definition $1/) {
			$state = "pastart";
			$paend = "Defined";
		}


		if ("" and
		    (/^Definition segment_of_action_with_res / or
		     /^Lemma propagates_correct / or
		     /^Lemma back_propagates_correct /)) {
			$paend = "Admitted";
		}

		print $res "$_\n";
	
		if ($state eq "pastart" && /\.$/) {
			$state = "";
			print $res "Proof.\n";
			print $res $impl;
			$impl = "";
			print $res "$paend.\n";
		}
	}
}
genmod($modname, $B, $M);
exit 0 unless $full;
close $B or die $!;
close $M or die $!;

open $M, "<", "src/$modname.v" or die $!;
genmod("${modname}Impl", $M, $Mi);
close $M or die $!;
close $Mi or die $!;

my $actiondef   = join("", map { my $a = $_; "\n| ${lcmodname}_action__$_ : ".join("", map { apartype($a, $_)." -> " } @{aparnames $_})."${lcmodname}_action" } @actions);
my $actionmatch = join("", map { my $a = $_; "| ${lcmodname}_action__$_".join("", map { " $_" } @{aparnames $_})." => ".arest($_)."\n  " } @actions);
my $actionparamtypedecs = join("", map { "  set (H_eq_${_}_dec := eq_${_}_dec).\n" } @actionparamtypes);
print $A <<endofactions
${actionparamtypeimports}Module ${modname}Actions
.

Inductive ${lcmodname}_action :=$actiondef.

Definition eq_${lcmodname}_action_dec :
  forall a1 a2 : ${lcmodname}_action,
    {a1 = a2}+{a1 <> a2}.
Proof.
${actionparamtypedecs}  decide equality.
Defined.

Definition ${lcmodname}_action_res
           (a : ${lcmodname}_action) :
  Set :=
  match a with
  ${actionmatch}end.

Definition eq_${lcmodname}_action_res_dec :
  forall (a : ${lcmodname}_action) (res1 res2 : ${lcmodname}_action_res a),
    {res1 = res2}+{res1 <> res2}.
Proof.
  intros.
  destruct a; simpl in *; decide equality.
Defined.

End ${modname}Actions.
endofactions
;
close $A or die $!;
#close $Ai or die $!;

my $implex = join("", map { "\n    _ <-- $lcmodname.$_".join("", map { " _(*$_*)" } @{aparnames $_}).";;" } @actions);
print $T <<endoftest
Require Beque.Util.common_extract.
Require Beque.Util.DelayExtractionDefinitionToken.

Require Import Beque.IO.impl.AxiomaticIOInAUnitWorld.
Require Import Beque.IO.intf.IOAux.

Require Import ${modname}Impl.

Extraction Language Haskell.

Module comext.
Include common_extract.commonExtractHaskell DelayExtractionDefinitionToken.token.
End comext.

Module ioext.
Include AxiomaticIOInAUnitWorld.extractHaskell DelayExtractionDefinitionToken.token.
End ioext.

Module ior := IOAux UnitWorld AxiomaticIOInAUnitWorld.
Import ior.

Module ${lcmodname}.
Include ${modname}Impl AxiomaticIOInAUnitWorld.UnitWorld AxiomaticIOInAUnitWorld.
Include ${lcmodname}.extractHaskell DelayExtractionDefinitionToken.token.
End ${lcmodname}.

Definition main :=$implex.

Extraction "test" main.
endoftest
;
close $T or die $!;

print $MF <<endofmakefile
COQPKG ?= Beque.Model.$modname

BEQUEINSTDIR ?= Beque\$(BEQUE_EXTRA)
BEQUE_EXTRA ?= \$(EXTRA)

COQROOT ?=
EDITOR ?= \$(COQROOT)coqide

SHELL ?= bash

COQC ?= \$(COQROOT)coqc
COQCFLAGS ?= `sed -e 's/#.*//' < _CoqProject`

COQCHK ?= \$(COQROOT)coqchk
COQCHKFLAGS ?= -o -silent

COQDEP ?= \$(COQROOT)coqdep
COQDEPFLAGS ?=

BASH ?= bash

TAR ?= tar
TARFLAGS ?=

DOT ?= dot
DOTFLAGS ?=

COQMAKEFILE ?= \$(COQROOT)coq_makefile
COQMAKEFILEFLAGS ?=

INSTALL ?= install
INSTALLFLAGS ?= -D -p -m 644

RUNHASKELL ?= runhaskell
RUNHASKELLFLAGS ?=

RUNOCAML ?= ocaml
RUNOCAMLFLAGS ?= -safe-string
OCAMLC ?= ocamlc
OCAMLCFLAGS ?= -g #OCAMLRUNPARAM=b for stack traces

GIT ?= git

REV ?= \$(shell \$(GIT) rev-parse HEAD)

COQDOC ?= \$(COQROOT)coqdoc
COQDOCFLAGS ?= --title '\$(COQPKG) - \$(shell \$(GIT) rev-parse HEAD)' --external https://beque.gitlab.io/core/ Beque `sed -e 's/\\#.*//' < _CoqProject`

PYTHON ?= python
PROJROOT ?= .
PROVIOLAROOT ?= \$(PROJROOT)/proviola
PROVIOLA ?= \$(PYTHON) \$(PROVIOLAROOT)/camera/camera.py
PROVIOLAFLAGS ?= --coqtop '\$(COQROOT)coqtop' --coq-project _CoqProject --shift-response --include-command --include-command-min-time-ms 50 --response-diff

XSLTPROC ?= xsltproc
XSLTPROCFLAGS ?=

MAJOR = 0
MINOR = 0
COMMITCOUNT = \$(shell \$(GIT) log --oneline | wc -l)
PATCH = \$(shell expr \$(COMMITCOUNT) - 0)
EXTRA ?= \$(shell git symbolic-ref --short HEAD | sed -e 's/^master\$\$//' -e 's/^./-&/')
DIRTY ?= \$(shell git status --short | tr -d \\\\n | sed -e 's/..*/_dirty-'`date +'%Y-%m-%d-%H:%M:%S'`'/')
VER = \$(MAJOR).\$(MINOR).\$(PATCH)\$(EXTRA)\$(DIRTY)

VFILES = \$(shell find src -path ./.bin -prune -o -name \\*.v -print | grep -v '\\\\\\#' | sed -e 's,^\\./,,') \$(shell test -e test && find test -path ./.bin -prune -o -name \\*.v -print | grep -v '\\\\\\#' | sed -e 's,^\\./,,')

VOFILES = \$(addprefix .bin/, \$(addsuffix o, \$(VFILES) \$(shell test -e test && find test -path ./.bin -prune -o -name \\*Haskell.v -print | grep -v '\\\\\\#' | sed -e 's,^\\./,,' -e 's/Haskell/Ocaml/g')))

V ?= true

src/%.v-check: .bin/src/%.vo
	\$(V) COQCHK \$*.v
	\$(COQCHK) \$(COQCHKFLAGS) `sed -e 's/#.*//' < _CoqProject` \$(COQPKG).\$(shell basename "\$*")

%.hs-run: %.hs
	\$(V) RUNHASKELL \$<
	\$(RUNHASKELL) \$(RUNHASKELLFLAGS) \$<

%.ml-run: %.ml
	\$(V) RUNOCAML \$<
	\$(RUNOCAML) \$(RUNOCAMLFLAGS) \$<

.bin/%.v: %.v
	mkdir -p \$(shell dirname \$@)
	ln -s ../\$(shell echo \$(dir \$<) | sed -e 's,^\\./,,' -e 's,/\\./,/,g' -e 's/[^/]\\+/../g')\$< \$@

%Ocaml.v: %Haskell.v
	\$(V) HASKELL2OCAML \$<
	sed -e 's/\\([^a-zA-Z ].*\\)Haskell/\\1Ocaml/' -e 's/ Haskell/ Ocaml/' < \$< > \$@

%.hs:
	\$(V) -n COQC HS \$<" "
	\$(COQC) \$(COQCFLAGS) \$<
	\$(V)

%.ml %.mli:
	\$(V) -n COQC ML \$<" "
	\$(COQC) \$(COQCFLAGS) \$<
	\$(V)

.bin/%.vo: .bin/%.v _CoqProject
	\$(V) -n COQC \$*.v" "
	\$(COQC) \$(COQCFLAGS) \$<
	\$(V)

%.htm: %.flm
	\$(V) XSLTPROC \$<
	\$(XSLTPROC) \$(XSLTPROCFLAGS) \$(PROVIOLAROOT)/proviola/coq/proviola.xsl \$< > \$@

%.flm: %.html _CoqProject
	\$(V) PROVIOLA \$<
	\$(PROVIOLA) \$(PROVIOLAFLAGS) \$< \$@

%.html: %.vo _CoqProject
	if [ ! -e \$(COQPKG).*\$(notdir \$@) -o \$< -nt \$(COQPKG).*\$(notdir \$@) ]; then \\
		\$(V) COQDOC \$<; \\
		\$(COQDOC) \$(COQDOCFLAGS) \$*.v -o \$@; \\
	else \\
		\$(V) MOVE COQDOC \$<; \\
		rm -f index.html; \\
		rm -f coqdoc.css; \\
		sed -e 's/This page has been generated by/\$(VER) - \$(shell date --utc --rfc-2822) - \$(COQPKG) - \$(shell git rev-parse --short=10 HEAD) - &/' < \$(COQPKG).*\$(notdir \$@) > \$@; \\
		rm \$(COQPKG).*\$(notdir \$@); \\
	fi

%.pp.ml: %.ml imports.ml.inc glue-string.ml.inc glue-trymap.ml.inc
	\$(V) PP ML \$*.pp.ml
	awk 'BEGIN { system("cat imports.ml.inc") } { print } /^. Err of .eRR/ { trymap += 1 } /^. String of char . string/ { trymap += 1; system("cat glue-string.ml.inc") } trymap == 2 { trymap += 1; system("cat glue-trymap.ml.inc") } /^let / { lastwasmain = 0 } /^let main =/ { lastwasmain = 1 } END { if (lastwasmain) { print ";; main ()" } }' < \$< > \$@

%.pp.hs: %.hs
	\$(V) PP HS \$*.pp.hs
	grep -q '{-# OPTIONS_GHC .* -cpp ' \$< || echo '{-# OPTIONS_GHC -cpp #-}' > \$@
	sed \\
		-e 's/import qualified Prelude/&\\n#include "imports.hs.inc"/' \\
		-e '\$\$ a #include "glue-string.hs.inc"\\n' \\
		-e '\$\$ a #include "glue-trymap.hs.inc"\\n' < \$< >> \$@

all: \$(VOFILES)
ver:
	echo \$(VER)

rev:
	echo \$(REV)

renders: docs \$(VOFILES:.vo=.htm)
docs: \$(VOFILES:.vo=.v) \$(VOFILES)
	\$(V) COQDOC \$(^:.vo=.v)
	\$(COQDOC) \$(COQDOCFLAGS) \$(^:.vo=.v)
	rm coqdoc.css index.html

dist-%:
	echo \$(COQPKG) \$(VER) \$(REV) \$* > \$(COQPKG)-\$(VER).txt
	\$(TAR) \$(TARFLAGS) --transform='s/\\.bin\\/src/\$(subst .,\\/,\$(COQPKG))/' --dereference -czf \$(COQPKG)-\$*-\$(VER).tar.gz \$(COQPKG)-\$(VER).txt \$(shell find .bin/src -name '*.\$*')
	rm \$(COQPKG)-\$(VER).txt

dist-with-test-%:
	echo \$(COQPKG) \$(VER) \$(REV) \$* > \$(COQPKG)-\$(VER).txt
	\$(TAR) \$(TARFLAGS) --transform='s/\\.bin\\/[^\\/]*/\$(subst .,\\/,\$(COQPKG))/' --dereference -czf \$(COQPKG)-\$*-\$(VER)-with-test.tar.gz \$(COQPKG)-\$(VER).txt \$(shell find .bin -name '*.\$*')
	rm \$(COQPKG)-\$(VER).txt

run: test.pp.hs-run test.pp.ml-run

validate: \$(addsuffix -check, \$(VFILES))
test: hw.hs
	\$(RUNHASKELL) \$(RUNHASKELLFLAGS) \$<

BEQUEVER ?= \$(shell cat \$(BEQUEINSTDIR)/version.txt)
skeleton:
	test ! -e TMPSKELETONOUT || (echo "review&remove TMPSKELETONOUT" && false)
	ddir=. BEQUEVER=\$(BEQUEVER) \$(BASH) \$(BEQUEINSTDIR)/ms.sh $modname "$actionbak" "$propbak" >TMPSKELETONOUT
	sed -n '\$\$p' TMPSKELETONOUT | grep -q '^[^ ]\\+\$\$' || (cat TMPSKELETONOUT && echo invalid branch name in last line of TMPSKELETONOUT >&2 && false)
	grep git.rebase.--onto TMPSKELETONOUT
	rm TMPSKELETONOUT

clean:
	find .bin -name \\*.vo -execdir rm {} +
	rm -f .coq-deps
	rm -f _CoqProject ._CoqProject.proofgeneral

_CoqProject ._CoqProject.proofgeneral: Makefile
	echo '# automatically generated, edit Makefile instead' > \$@
	echo -R \$(BEQUEINSTDIR) Beque >>\$@
	echo -R .bin/src \$(COQPKG) >> \$@
	if test -e test; then echo -R .bin/test \$(COQPKG) >> \$@; else true; fi
	# proofgeneral workaround: PG reverses the order of arguments when passing to coqtop, set coq-project-filename to "._CoqProject.proofgeneral" in your emacs config
	tac \$@ > .\$@.proofgeneral

.coq-deps: Makefile _CoqProject \$(VFILES)
	test -e .bin/src || mkdir -p .bin/src
	test -e .bin/test || mkdir -p .bin/test
	\$(COQDEP) \$(COQDEPFLAGS) `sed -e 's/#.*//' < _CoqProject | grep -v -- -R..bin/test.\$(COQPKG) | grep -v -- -R..bin/src.\$(COQPKG)` -R src \$(COQPKG) \$(shell test -e test && echo -R test \$(COQPKG)) \$(VFILES) > \$@.tmp.0
	sed -e 's, src/, .bin/src/,g' -e 's, test/, .bin/test/,g' -e 's,^src/,.bin/src/,' -e 's,^test/,.bin/test/,' -e 's, \\./\\.bin/, .bin/,g' > \$@.tmp < \$@.tmp.0
	rm \$@.tmp.0
	for v in \$(VFILES); do for e in `sed -n -e '/^Extraction "/ { s/.*"\\([^"]*\\)".*/\\1/; p }' < "\$\$v"`; do sed -i /"`echo "\$\$v" | sed -e 's,/,.g,'`o"' / s/^/'\$\$e'.hs /' \$@.tmp; done; done
	grep Haskell.vo\\  \$@.tmp | sed -e 's/Haskell/Ocaml/g' -e 's/\\([^ ]*\\).hs /\\1.ml \\1.mli /g' > \$@.tmp.ocaml
	grep . \$@.tmp.ocaml >> \$@.tmp
	rm \$@.tmp.ocaml
	mv \$@.tmp \$@

-include .coq-deps
endofmakefile
;
close $MF or die $!;

print $GL <<endofgitlabci
# see https://hub.docker.com/_/debian/
build:
  stage: build
  image: registry.gitlab.com/mgshrd/beque-build-image/beque-build:latest
  before_script:
    - wget https://gitlab.com/mgshrd/proviola/builds/artifacts/master/download?job=build -O artifacts.zip && unzip artifacts.zip && rm artifacts.zip && mkdir proviola && tar xf proviola-*.tar.gz -C proviola && rm proviola-*.tar.gz
    - (wget https://gitlab.com/mgshrd/beque/builds/artifacts/`git symbolic-ref --short HEAD`/download?job=build -O artifacts.zip || wget https://gitlab.com/mgshrd/beque/builds/artifacts/master/download?job=build -O artifacts.zip) && unzip artifacts.zip && rm artifacts.zip && rm beque-doc-*.tar.gz && rm beque-src-*.tar.gz && rm beque-all-*.tar.gz && rm beque-misc-*.tar.gz && tar xf beque-bin-*.tar.gz && rm beque-bin-*.tar.gz
    - coqc -v
    - echo 'Require Import Beque.beque_revision. Print beque_revision.'  | coqtop -R Beque Beque
  script:
    - make ver
    - time make SHELL="time bash -c" test.pp.hs
    - time make SHELL="time bash -c" test.pp.ml
    - time make SHELL="time bash -c" all
    - time make SHELL="time bash -c" dist-vo
    - time make SHELL="time bash -c" dist-v
    - time make SHELL="time bash -c" docs
    - time make SHELL="time bash -c" renders
    - time make SHELL="time bash -c" dist-htm
    - mkdir .bin/all; for a in Beque.Model.$modname-*.tar.gz; do tar -C .bin/all -zxvf \$a; done; tar -C .bin/all -czf Beque.Model.$modname-all-`make -s ver`.tar.gz .; rm -rf .bin/all
  artifacts:
    paths:
      - imports.hs.inc
      - glue-string.hs.inc
      - glue-trymap.hs.inc
      - test.pp.hs
      - test.pp.ml
      - Beque.Model.$modname-*.tar.gz

test-haskell:
  stage: test
  image: registry.gitlab.com/mgshrd/beque-build-image/beque-test-haskell:latest
  script:
    - rm *.tar.gz
    - sha256sum test.pp.hs
    - runhaskell --version
    - runhaskell test.pp.hs

test-ocaml:
  stage: test
  image: registry.gitlab.com/mgshrd/beque-build-image/beque-test-ocaml:latest
  script:
    - rm *.tar.gz *.hs *.hs.inc
    - sha256sum test.pp.ml
    - ocaml -version
    - ocaml -safe-string test.pp.ml
endofgitlabci
;
close $GL or die $!;


exit 0; #skip embedding
print $E <<endofembed
Require Import Beque.IO.io.World.

Require Import ${modname}Actions.

Module Type ${modname}Embed
       (w : World)
.

Parameter to_fd :
  list w.msg ->
  option {a : ${lcmodname}_action & fda.${lcmodname}_action_res a}.

End ${modname}Embed.
endofembed
;
close $E or die $!;
