<center>Beque: Certified I/O Programming with Coq</center>
========================================================

Beque is a library for Coq that is intended to be a "certifiable IO monad", using event trace based Hoare logic.

Main goals:

- correctness: prove that the world[<a href="#haskio">haskio</a>] is in a some state
- composability: provide tools to mix and match different components to create the world<a name="fnback1">[<sup>1</sup>](#fn1)
- extensibility: allow creation and integration of new components

<center>1. Motivation</center>
----------------------------------------------

As I delved more and more into developing [certified software](http://adam.chlipala.net/cpdt/) with Coq, I started missing the possibility to communicate the results to/get inputs from the outside world. Since I was otherwise very much enjoying the tools and the development environment, I want to try to scratch this itch, instead of moving to a different tool.

Ynot[<a href="#ynot1">ynot1</a>][<a href="#ynot2">ynot2</a>] does provide a framework that can incorporate IO([<a href="#ynottr">ynottr</a>] & [quark](http://goto.ucsd.edu/quark/)), but adding IO on top of the underlying heap centric logic seems backwards to me.

[Idris](http://www.idris-lang.org/) is another project targeting similar goals, but it abandons the ecosystem built up around Coq.

Haskell's IO monad[<a href="#haskio">haskio</a>] is implicitly talking to an abstract world: "Provided that the primitive ccall actions are combined only with these combinators, we can guarantee that the ccalls will be linked in a single, linear chain[...]".

Here I propose a new library, taking what we can from Ynot's and Idris' ideas, but applying them in Coq to a different basic logic. A logic that traces the actions in the "linear chain", and applies Hoare style reasoning to the trace.

<center>2. Introducing T-HTT</center>
----------------------------------------------

The logic the library is based on is similar to Hoare Type Theory[<a href="#sephtt">sephtt</a>], but uses a message trace to represent the state and temporal logic<a name="fnback2">[<sup>2</sup>](#fn2) instead of separation logic to express properties of the state.




### 2.1 Trace of messages

Each message is a synchronous request/response exchange with the world<a name="fnback3">[<sup>3</sup>](#fn3). The totality of the messages already exchanged is the trace.

As a first attempt, only ordered finite list of messages is supported, leaving infinite streams of messages (e.g. for never stopping services) and in-/finite DAGs (e.g. for tracing parallel execution of actions) of messages for later investigation.

The messages in the trace are ordered in decreasing time of exchange that is the newest message is at the head of the trace.




### 2.2 Actions

An action is the usual Hoare triple `(pre, A, post)`, where the action is enabled if the current trace is conforming to `pre`. After executing the action a value of `A` is produced, while the trace is prepended with new messages, and now it conforms to `post`.

`IO` in the [prototype](src/lib/core/IOAux.v#L21).

In pseudo Coq code:

```coq
IO :
  forall
    (pre : trace -> Prop)
    (A : Set)
    (post : A -> segment -> trace -> Prop),
  Type
```
where

In the postcondition `segment` is the type of finite ordered messages exchanged with the world during the execution of the action, the last argument of type `trace` is a value satisfying `pre` (the old trace). The new trace after action execution equals to the segment prepended to the old trace.


#### 2.2.1 Sequencing actions

Executing an action based on the result of another action is done using dependent `bind`.

In the [prototype](src/lib/core/IOAux.v#L33):

```coq
bind :
  IO pre A post_a ->
  (f : forall (a : A) (s : segment), IO (post_a a s) (B a) (post_b a)) ->
  IO
    pre
    {a : A & B a}
    (fun (ab : {a : A & B}) (s : segment) (t : trace) =>
      exists sb sa, sb ++ sa = s /\
        post_a (proj1 ab) sa t /\
        (post_b (proj1 ab) sa) (proj2 ab) sb (sa ++ t))
```

The rule, requires the first action's postcondition to be the second actions precondition, leaving the necessary strengthening and weakening to be explicitly specified.



### 2.3 Propositions on traces

A trace of actions is a common theme in the literature LTL[<a href="#ltl">ltl</a>] is the most obvious candidate, but CSP tools[<a href="#csp">csp</a>], emergence logic[<a href="#el">el</a>], or process algebra[<a href="#pacoc">pacoc</a>] are also avenues worth investigating.


#### 2.3.1 Messages from the future
Propositions can only refer to past messages, because of this it is not possible for example for the `open` action to require, that the returned filehandle be eventually closed.

Using continuations may mitigate this problem somewhat, e.g. `open` could specify that it will only pass the filehandle to a continuation, that eventually closes it.




<center>3. Composability</center>
-----------------------------------------

Different systems/resources/etc require different messages, and different programs want to use different subset of resources.

A domain represents such a resource, it defines messages and actions. The world is composed of domain instances and it has its own message type, the world's message is used to define the trace.

The world also provides a function, [mapping world messages](src/lib/core/MessageMapper.v) to messages of each of its domains. The domains define their action pre-/postconditions in terms of the trace, that is in world messages, and use the world provided mapping function to map the abstract world messages to their own concrete domain message [type](src/lib/domains/ff/FlipFlop.v).




### 3.1 Trace property propagation

Each property on a trace is invalidated by adding a single message to the trace, that is `P t` does not imply `P (m::t)` or `P (s ++ t)`. To make this model usable, there needs to be a way to express, that some messages don't interfere with some properties.


#### 3.1.1 Domain internal property propagation

How one domain wants its messages to interact with trace properties, is left up to the domain. E.g. the heap domain may use separation logic to express property propagation, while the filesystem domain may require file locking to allow properties describing the file contents to propagate.


#### 3.1.2 Cross domain property propagation

Properties form different domains may or may not interfere with each other, it is the responsibility of the world, to declare which domains are independent, or which properties of which domains are.

To easily define that all properties coming from domain `D`<sub>`i`</sub> we need a declaration, that property `P` is from domain `D`<sub>`i`</sub>. In the [prototype](src/lib/core/MessageMapperUtil.v#L20).


<center>4. Extensibility</center>
-----------------------------------------

As domains are not aware of the world's specific layout (they only see the world through the mapping&framing provided by the world), new domains can be added to a world, as long as they don't interfere with the existing domains, i.e. all the new domains actions are out of frame for the existing domains. If the new domain influences the existing domains, the old implementations will fail to compile, as they should.

The necessary framing must be defined in the world with the tools introduced in the previous section.

<center>5. Goals</center>
-------------------------------------

### 5.1 Minimal goals

The fulfillment of the minimal goals would mean, that Coq developments can take advantage of a persistent store and a way of communicating with the outside world.

1. Domain composition and trace framing.
   - Using the domain internal and declared cross domain property propagation rules, prove that some higher level property also propagates over some messages.
1. Domain: key-value store
   - which side serializes? I.e mapping bit-strings-to-bit-strings vs. requiring the de-/serialization function to be provided
1. Domains: sending channel & receiving channel
   - similar serialization question as above, plus channel managed message boundary vs. pushing the requirement of correct handling to the received de-/serialization functions?
1. Create tutorial and documentation on how to use the library, create domains, and define worlds.

### 5.2 Goals

The coverage of these goals would mean that we have domains used by realistic systems, a nice way to talk about trace properties and theoretical assurance that the design is not suffering some hidden inconsistency.

1. Adapt temporal logic to express trace propositions
1. Create theoretical foundations by adapting Hoare Type Theory[<a href="#sephtt">sephtt</a>] to traces
1. network sockets:
   - listen
   - accept
   - connect
1. file system (enriching the k-v store)
   - persistence attributes:
     - persists as long as the process
     - persists as long as some other resource
   - availability attribute
   - aliasing, advisory/mandatory locking
1. file system2 (certified fs)
   - create a block device domain
   - create a certified implementation of the file system API on top of the block device
1. heap
   - independent memory pools should come free as separate heap domain instances
1. Custom property propagation example
1. Embedded domain, aka domain in domain
   - remote key-value store, i.e. send k-v store messages through the channels
   - The prototype uses [`domainid`](https://gitlab.com/mgshrd/beque/blob/parametric-domains/src/lib/core/World.v) in world to identify embedded domains, it would be better to not expose every component to this detail.
1. Embedded worlds, aka world in a domain
   - is there systematic way of doing this?
1. Is monad necessary? Investigate using a weaker type, and learn from the Haskell [discussion](http://stackoverflow.com/questions/6564749/is-applicative-io-implemented-based-on-functions-from-monad-io).



### 5.3 Extended goals

The goals here are possible further extension, that are considered optional or too far fetched.

1. investigate continuation passing as a vehicle of expressing constraints about the future
1. parallel execution
   - map DAG to loose traces
   - As a first approximation, parallel composition could be allowed, if both actions can guarantee their own postcondition, in face of all possible intermingling of the other actions messages.

    ```coq
    bind :
      IO pre_a A post_a ->
      IO pre_b B post_b ->
      IO
        (fun t =>
          pre_a t /\
          pre_b t /\
          forall a sa b sb s,
            post_a a sa t ->
    	post_b b sb t ->
    	In s (all_interleaves sa sb) ->
    	(post_a a s t /\ post_b b s t))
        (A * B)
        (fun (ab : A * B) (s : segment) (t : trace) =>
          post_a a s t /\ post_b b s t)
   ```
1. add separation logic to the heap domain
1. observability and hiding in CSP: investigate, as any unobservable messages are necessarily not influencing, the tools from CSP might help in framing.
1. add `select` to network sockets
1. expose a Ynot implemented datastructure via a message passing interface
1. investigate point-free IO, that is, remove `return` and `bind`, and only allow Kleisli composition of IO values (`IO -> IO -> IO`). This would further decrease the mandatory base, and the "internal computation"[<a href="#haskio">haskio</a>] could be separated into its own domain (the "vm domain").
   - each message can be a CPU instruction, and the state managed by the messages the registers
1. minimal OS interface, where each syscall is a message
   - "microkernel app": implement privilege separation environment a'la [pledge](http://man.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man2/pledge.2) subprocess approach (e.g. [ntp](https://www.openbsd.org/papers/dot2016.pdf): net, dns, timemgmt)
1. lossy channel
1. transactions: with domain opt-in or mandatory?
1. improve coq
   - metaprogramming ability: add to actions defined in a module receiving [message mappers](src/lib/core/MessageMapper.v), that they only produce messages in the frames [defined](src/lib/core/TraceFrame.v) by the mappers.
   - allow default implementation to come from `Module Type`
   - extraction improvements
1. realtime
1. crypto, e.g nonce freshness
1. trace invertibility: the goal of composability and extensibility prevents defining the trace, as below, but the inversion of such definition could be useful, investigate recovering it.

```coq
Section closed_world.

  Variable msg1_data : Set.
  Variable msg2_data : Set.

  Inductive msg :=
  | msg1_ctor : msg1_data -> msg
  | msg2_ctor : msg2_data -> msg.

  Variable msg1_pre : msg1_data -> list msg -> Prop.
  Variable msg2_pre : msg2_data -> list msg -> Prop.

  Inductive trace : list msg -> Prop :=
  | empty_trace : trace nil
  | msg1_cons : forall l' (t' : trace l') (d : msg1_data), msg1_pre d l' -> trace (msg1_ctor d::l')
  | msg2_cons : forall l' (t' : trace l') (d : msg2_data), msg2_pre d l' -> trace (msg2_ctor d::l').

End closed_world.
```


<center>6. Prototype</center>
-------------------------------------

The prototype demonstrates some of the ideas described here. At the moment it is based on modules, but I'm considering moving to records if modules turn out to be too cumbersome.

The `master` branch contains the most basic features, mostly covers goal 5.1.1.

`abstract-domain` demonstrates how to work with a domain, that abstracts away it's implementation.

`ltl-negation-safety` is an attempt to safely handle LTL property operations in face of out of frame messages. It is broken at the moment. Targets 5.2.1.

`parametric-domains` demonstrates a domain managing a set of instances of another domain, precursor for 5.2.8. It is broken at the moment.

`case-handling` demonstrates a way to handle conditional actions, that depend on the result of another action.

### 6.1 Trying it out

The minimum requirement is Coq 8.5 to compile the prototype, and GHC to run the toy apps.

The development environment depends on the standard unix utilities `make`, `find`, `grep`, `cat`, `xargs`.

#### 6.1.1 Example

To run the two examples:

```sh
$ make src/examples/ProjectFileSingleFlipFlopWorld.vo # this will generate single_flipflop.hs
$ runhaskell single_flipflop.hs
$ make src/examples/ProjectFileTwoFlipFlopsWorld.vo # this will generate two_flipflops.hs
$ runhaskell two_flipflops.hs
```

To edit a `.v` file in `coqide`, append `-edit` to its name and `make` it:

```sh
$ make src/lib/domains/ff/FlipFlop.v-edit # this will fully compile the file and its dependencies before opening it in coqide
```

### 6.2 Tool configuration

#### 6.2.1 Git

To get hints about which `Definition` contains the hunk in `git diff`'s output, put the following in the repo's `.git/config`
```
[diff "coq"]
	xfuncname = "^ *(([A-OQ-Z][a-z]|P[^r][^o][^o][^f]).* .*)"
```

and in `.git/info/attributes` this
```
*.v	diff=coq
```

#### 6.2.2 emacs

In your `(custom-set-variables` section add:
```
 '(vc-follow-symlinks t)
```
to get rid of the emacs' annoying conformation question at file open through symlink (which happens, because of the separation of compilation results).

References
----------
<a name="sephtt">sephtt</a>:  <sup>[link](http://ynot.cs.harvard.edu/papers/HTTmodel.pdf)</sup>
 Rasmus Lerchedahl Petersen, Lars Birkedal, Aleksandar Nanevski, and Greg Morrisett.
 _A realizability model for impredicative hoare type theory._
 In Sophia Drossopoulou, editor, European Symposium on Programming 2008, volume 4960 of Lecture Notes in Computer Science, pages 337-352. Springer, 2008.

<a name="ynot1">ynot1</a>:  <sup>[link](http://ynot.cs.harvard.edu/papers/ynot08.pdf)</sup>
 Aleksandar Nanevski, Greg Morrisett, Avraham Shinnar, Paul Govereau, and Lars Birkedal.
 _Ynot: Reasoning with the awkward squad._
 Proc. ICFP, 2008.

<a name="ynot2">ynot2</a>:  <sup>[link](http://ynot.cs.harvard.edu/papers/icfp09.pdf)</sup>
 Adam Chlipala, Gregory Malecha, Greg Morrisett, Avraham Shinnar, and Ryan Wisnesky.
 _Effective interactive proofs for higher-order imperative programs._
 In Proceedings of the 14th ACM SIGPLAN International Confernence on Functional Programming, 2009.

<a name="ynottr">ynottr</a>:  <sup>[link](http://ynot.cs.harvard.edu/papers/jsc-wwv-10.pdf)</sup>
 Gregory Malecha, Greg Morrisett, Ryan Wisnesky.
 _Trace-based Verification of Imperative Programs with I/O._
 Preprint, 2010.

<a name="haskio">haskio</a>:  <sup>[link](http://research.microsoft.com/en-us/um/people/simonpj/Papers/imperative.ps.Z)</sup>
 Simon L. Peyton Jones and Philip Wadler.
 _Imperative functional programming._
 In POPL '93: Proceedings of the 20th annual ACM SIGPLAN-SIGACT symposium on Principles of programming languages, pages 71-84, New York, NY, USA. ACM.
 1993.

<a name="ltl">ltl</a>: 
 E. A. Emerson.
 _Temporal and modal logic._
 In J. van Leeuwen, editor, Formal Models and Semantics, volume B of Handbook of Theoretical Computer Science, pages 995-1072. MIT Press, Cambridge, Massachusetts, Jan. ISBN 978-0262720151.
 1994.


<a name="csp">csp</a>:  process calculus esp. traces aka  synchronization trees, HOARE, C. A. R. 1985. Communicating Sequential Processes. Series in Computer Science.  Prentice-Hall.

<a name="el">el</a>: 
 Wolfgang Jeltsch.
 _Temporal Logic with "Until", Functional Reactive Programming with Processes, and Concrete Process Categories._
 2013.

<a name="pacoc">pacoc</a>: 
 Marc Bezem, Jan Friso Groote, and Roland Bol.
 _Formalizing Process Algebraic Verications in the Calculus of Constructions._
 1997.

Footnotes
---------

<p><a name="fn1">1: <a href="#fnback1"><sup>^</sup></a> components are called `Domain` in the [prototype](src/lib/core/Domain.v)<p>, analoguous to algebras [here](http://degoes.net/articles/modern-fp).
<a name="fn2">2: <a href="#fnback2"><sup>^</sup></a> LTL[<a href="#ltl">ltl</a>] is the most obvious candidate, but CSP tools[<a href="#csp">csp</a>], emergence logic[<a href="#el">el</a>], or process algebra[<a href="#pacoc">pacoc</a>] are also avenues worth investigating.<p>
<a name="fn3">3: <a href="#fnback3"><sup>^</sup></a> `w` in [<a href="#haskio">haskio</a>]<p>
