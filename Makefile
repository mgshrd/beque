GIT ?= git
GITFLAGS ?=

COQC ?= $(COQROOT)coqc
COQCFLAGS ?=

BEQUEINSTDIR ?=  ./Beque$(EXTRA)
SUBTARGET ?= install

MAJOR ?= 0
MINOR ?= 2
COUNTBASECOMMIT = 8f78174f56264f311876d8641b162c3c9290746e
COMMITCOUNT ?= $(shell $(GIT) rev-list --count $(COUNTBASECOMMIT)..HEAD)
PATCH ?= $(shell expr $(COMMITCOUNT) - 4)
EXTRA ?=  $(shell git symbolic-ref --short HEAD | sed -e 's/^master$$//' -e 's/^./-&/')
NOW :=  `date +'%Y-%m-%d-%H-%M-%S'`
DIRTY ?=  $(shell git status --short | tr -d \\n | sed -e 's/..*/_dirty-'$(NOW)'/')
VER ?= $(MAJOR).$(MINOR).$(PATCH)$(EXTRA)$(DIRTY)
REV ?= $(shell git rev-parse --short=10 HEAD)

V ?= true
VV ?= $(V)

build-util-%:
	$(VV) STARTSUBMAKE $* util
	$(MAKE) VER=$(VER) REV=$(REV) BEQUEINSTDIR=$(abspath $(BEQUEINSTDIR)) -C util $*
	$(V) DONESUBMAKE $* util

build-io-%: build-util-%
	$(VV) STARTSUBMAKE $* io
	$(MAKE) VER=$(VER) REV=$(REV) BEQUEINSTDIR=$(abspath $(BEQUEINSTDIR)) -C io $*
	$(V) DONESUBMAKE $* io

build-ltl-%: build-io-% build-util-%
	$(VV) STARTSUBMAKE $* ltl
	$(MAKE) VER=$(VER) REV=$(REV) BEQUEINSTDIR=$(abspath $(BEQUEINSTDIR)) -C ltl $*
	$(V) DONESUBMAKE $* ltl

build-frame-%: build-ltl-% build-io-% build-util-%
	$(VV) STARTSUBMAKE $* frame
	$(MAKE) VER=$(VER) REV=$(REV) BEQUEINSTDIR=$(abspath $(BEQUEINSTDIR)) -C frame $*
	$(V) DONESUBMAKE $* frame

build-%: $(BEQUEINSTDIR) build-util-% build-io-% build-ltl-% build-frame-%
	$(V) SUBMAKE $* done

build-verbose-%: $(BEQUEINSTDIR)
	$(V) SUBMAKE $*
	$(MAKE) V=echo VER=$(VER) REV=$(REV) BEQUEINSTDIR=$(abspath $(BEQUEINSTDIR)) -C util $*
	$(MAKE) V=echo VER=$(VER) REV=$(REV) BEQUEINSTDIR=$(abspath $(BEQUEINSTDIR)) -C io $*
	$(MAKE) V=echo VER=$(VER) REV=$(REV) BEQUEINSTDIR=$(abspath $(BEQUEINSTDIR)) -C ltl $*
	$(MAKE) V=echo VER=$(VER) REV=$(REV) BEQUEINSTDIR=$(abspath $(BEQUEINSTDIR)) -C frame $*

all: build-install build-install-src build-vlint $(BEQUEINSTDIR)/version.txt $(BEQUEINSTDIR)/version-$(VER)-revision.txt

$(BEQUEINSTDIR)/version.txt:
	echo $(VER) > $@

$(BEQUEINSTDIR)/version-$(VER)-revision.txt:
	echo $(shell git rev-parse HEAD) > $@

dist: build-install $(BEQUEINSTDIR)/beque_revision.vo $(BEQUEINSTDIR)/version-$(VER)-revision.txt
	$(V) beque-bin-$(VER).tar.gz
	tar --exclude=*.v --exclude=*.htm -czf beque-bin-$(VER).tar.gz $(BEQUEINSTDIR)

build-docs: build-install
build-test: build-install
build-docs-with-test: build-test
build-install-render-with-test: build-docs-with-test
docs: build-install-render-with-test
doc-dist: docs
	$(V) beque-doc-$(VER).tar.gz
	tar --exclude=*.v --exclude=*.vo -czf beque-doc-$(VER).tar.gz $(BEQUEINSTDIR)

src-dist: build-install-src-with-test
	$(V) beque-src-$(VER).tar.gz
	tar --exclude=*.vo --exclude=*.htm -czf beque-src-$(VER).tar.gz $(BEQUEINSTDIR)

misc-dist:
	$(V) beque-misc-$(VER).tar.gz
	sed -e 's/\$$BEQUEVER/$(VER)/g' < ms.sh > ms.sh.fixdver
	tar --transform=s/.fixdver// --transform=s/^/.\\/Beque\\// -czf beque-misc-$(VER).tar.gz vlint.pl ms.pl ms.sh.fixdver plomp.sh
	rm ms.sh.fixdver

all-dist:
	$(V) beque-all-$(VER).tar.gz
	mkdir all.tmp
	tar -C all.tmp -zxf beque-bin-$(VER).tar.gz
	tar -C all.tmp -zxf beque-doc-$(VER).tar.gz
	tar -C all.tmp -zxf beque-src-$(VER).tar.gz
	tar -C all.tmp -zxf beque-misc-$(VER).tar.gz
	tar -C all.tmp -czf beque-all-$(VER).tar.gz .

ver:
	echo $(VER)

rev:
	echo $(REV)

ci: ver dist doc-dist src-dist

try-uninstall:
	$(MAKE) BEQUEINSTDIR=$(abspath $(BEQUEINSTDIR)) -C util uninstall || true
	$(MAKE) BEQUEINSTDIR=$(abspath $(BEQUEINSTDIR)) -C io uninstall || true
	$(MAKE) BEQUEINSTDIR=$(abspath $(BEQUEINSTDIR)) -C ltl uninstall || true
	$(MAKE) BEQUEINSTDIR=$(abspath $(BEQUEINSTDIR)) -C frame uninstall || true

clean: try-uninstall build-clean
	rm -rf stats
	rm -f $(BEQUEINSTDIR)/beque_revision.vo

$(BEQUEINSTDIR):
	mkdir -p $@

$(BEQUEINSTDIR)/beque_revision.vo: $(BEQUEINSTDIR)
	echo 'Inductive beque_revision := beque_revision_$(shell $(GIT) rev-parse HEAD).' > $(BEQUEINSTDIR)/beque_revision.v
	$(COQC) $(COQCFLAGS) -R $(BEQUEINSTDIR) Beque $(BEQUEINSTDIR)/beque_revision.v
	mv $(BEQUEINSTDIR)/beque_revision.vo $(BEQUEINSTDIR)/beque_revisionx.vo
	rm $(BEQUEINSTDIR)/beque_revision.* $(BEQUEINSTDIR)/.beque_revision.*
	mv $(BEQUEINSTDIR)/beque_revisionx.vo $(BEQUEINSTDIR)/beque_revision.vo
