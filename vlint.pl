use strict;

my $errc = 0;
my $ll = "";
my $ln = 0;
my $state = "noass";
my $expectfin = "";

sub err {
	my $file = shift;
	my $line = shift;
	my $msg = shift;
	print STDERR "$file:$line: $msg\n";
	$errc++;
}
while (<>) {
	if (s/^(\(\* resolved )?#importashead //) {
		s/\s*\*\).*//; # drop noise
	} elsif (/^(\(\* resolved )?#import/) {
		next;
	} 
	if (/^\s*Goal/ or /^\s*Theorem / or /^\s*Lemma / or /^\s*Remark / or /^\s*Fact / or /^\s*Corollary / or /^\s*Proposition / or /^\s*Example / or /^\s*Fixpoint /) {
		$expectfin = "Qed";
		$state = "asshead";
	}
	if (/^\s*Definition / or /^\s*Fixpoint /) {
		$expectfin = "Defined";
		$state = "asshead";
	}
	if (/^\s*Proof\./) {
		if ($state ne "asshead") {
			err $ARGV, $., "unexpected start of proof";
		}
		$state = "asspf";
	}
	if (/\b(Defined)\./ or /\b(Qed)\./ or /\b(Admitted)\./ or /\b(Abort)\./ or /\bSave /) {
		if ($1 ne "Admitted" and $1 ne "Abort" and $1 ne $expectfin) {
			err $ARGV, $., "expected $expectfin found $1";
		}
		if ($state ne "asspf") {
			err $ARGV, $., "proof close $1 found, but proof was not started";
		}
		$state = "noass";
	}
	if (/\s\n$/) {
		err $ARGV, $., "whitespace at the end of line";
	}
	if (!/\n$/) {
		err $ARGV, $., "no newline at the end of line";
	}
	if (/^\t/) {
		err $ARGV, $., "no tabs allowed";
	}
	$ll = $_;
	$ln = $.;
}
my $lx = $ll;
chomp $lx;
if ($lx eq "") {
	err $ARGV, $., "empty last line";
}
if ($ll eq $lx) {
	err $ARGV, $., "last line has no new-line";
}
die "$errc error(s) found" if $errc != 0;
